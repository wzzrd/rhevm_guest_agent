class rhevm_guest_agent::params {

  if $::operatingsystemmajrelease == 7 {
    $qemu_service_name = 'qemu-guest-agent'
  } elsif $::operatingsystemmajrelease == 6 {
    $qemu_service_name = 'qemu-ga'
  }

}
