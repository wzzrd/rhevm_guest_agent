# Class: rhevm_guest_agent::config
# ===========================
#
# Authors
# -------
#
# Maxim Burgerhout <maxim@wzzrd.com>
#
# Copyright
# ---------
#
# Copyright 2016 Maxim Burgerhout, unless otherwise noted.
#
class rhevm_guest_agent::config {

  file { '/etc/ovirt-guest-agent.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template('rhevm_guest_agent/ovirt-guest-agent.conf.erb'),
    require => Package['rhevm-guest-agent-common'],
  }  

  file { '/etc/sysconfig/qemu-ga':
    ensure => file,
    owner  => 'root',
    group   => 'root',
    mode    => '0644',
    content => template("rhevm_guest_agent/qemu-ga.${::operatingsystemmajrelease}.erb"),
    require => Package['qemu-guest-agent'],
  }

}
