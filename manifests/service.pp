# Class: rhevm_guest_agent::service
# ===========================
#
# Authors
# -------
#
# Maxim Burgerhout <maxim@wzzrd.com>
#
# Copyright
# ---------
#
# Copyright 2016 Maxim Burgerhout, unless otherwise noted.
#
class rhevm_guest_agent::service {

  service { 'ovirt-guest-agent':
    ensure    => running,
    enable    => true,
    subscribe => File['/etc/ovirt-guest-agent.conf'],
  }

  service { "$::rhevm_guest_agent::params::qemu_service_name":
    ensure    => running,
    enable    => true,
    subscribe => File['/etc/sysconfig/qemu-ga'],
  }

}
