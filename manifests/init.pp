# Class: rhevm_guest_agent
# ===========================
#
# Full description of class rhevm_guest_agent here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'rhevm_guest_agent':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class rhevm_guest_agent(
  $plugins = $::rhevm_guest_agent::plugins,
) inherits rhevm_guest_agent::params {

  if $::productname == 'RHEV Hypervisor' {

    anchor { 'rhevm_guest_agent::begin': } ->                                                    
    class { '::rhevm_guest_agent::install': } ->                                                 
    class { '::rhevm_guest_agent::config': } ~>                                                  
    class { '::rhevm_guest_agent::service': } ->                                                 
    anchor { 'rhevm_guest_agent::end': }  

  }

}
